// LIBRERIAS a utilizar -------------------------------------------------------

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <FirebaseESP8266.h>


// Declaro VARIABLES -------------------------------------------------------

int contconexion = 0;

char ssid[50];      
char pass[50];

const char *ssidConf = "Sensor MQ-2";
const char *passConf = "1234567890";

String mensaje = "";

#define PinAnalogico A0  
#define PinDigital D7
#define Buzzer 5
#define Boton 14


// Declarando las FUNCIONES a utilizar ------------------------------------------------------- 

void setup_wifi();
void modoconf();
void guardar_conf();
void grabar(int, String);
String leer(int);
void escanear();


// FIREBASE -------------------------------------------------------

#define FIREBASE_HOST "https://project-1511549768840229133-default-rtdb.firebaseio.com/"
#define FIREBASE_AUTH "F57zt4TFxVtYATXpTdnYAUT8XWc3fXxME56vpCu5"

FirebaseData firebaseD; // Definimos el objeto de data para FIREBASE.
String nodo = "Proyecto" ; //Definimos una ruta de Firebase.


// Pagina en HTML -------------------------------------------------------
 
String pagina = "<!DOCTYPE html>"
"<html lang='en'>"
"<head>"
    "<meta charset='UTF-8'>"
    "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"
    "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"
    "<title>Document</title>"
    "<style>"
".form__group {position: relative;padding: 15px 0 0;margin-top: 10px;width: 50%;}"

".form__field {font-family: inherit;width: 100%;border: 0;border-bottom: 2px solid #9b9b9b;outline: 0;font-size: 1.3rem;color: #fff;padding: 7px 0;background: transparent;transition: border-color 0.2s;}"
".form__field::placeholder {color: transparent;}"
".form__field:placeholder-shown ~ .form__label {font-size: 1.3rem;cursor: text;top: 20px;}"

".form__label {position: absolute;top: 0;display: block;transition: 0.2s;font-size: 1rem;color: #9b9b9b;}"

".form__field:focus {padding-bottom: 6px;font-weight: 700;border-width: 3px;border-image: linear-gradient(to right, #11998e, #38ef7d);border-image-slice: 1;}"
".form__field:focus ~ .form__label {position: absolute;top: 0;display: block;transition: 0.2s;font-size: 1rem;color: #11998e;font-weight: 700;}"

/* reset input */
".form__field:required, .form__field:invalid {box-shadow: none;}"

/* demo */
" body {font-family: 'Poppins', sans-serif;display: flex;flex-direction: column;justify-content: center;align-items: center;min-height: 100vh;font-size: 1.5rem;background-color: #222222;}"

".box{background: linear-gradient(to bottom right, #EF4765, #FF9A5A);border-radius:3px;padding:50px 70px;margin-top: 20px;}"

".P{font-family: inherit;}"

    "</style>"
"</head>"
"<body style='  font-family: 'Poppins', sans-serif; display: flex;flex-direction: column;justify-content: center;align-items: center;min-height: 50vh;font-size: 1.5rem;background-color:#222222;''>"
    "<form action='guardar_conf' method='get'>"
        "<input type='text' style='font-family: inherit;width: 100%;border: 0;border-bottom: 2px solid gray;outline: 0;font-size: 1.3rem;color: white;padding: 7px 0;background: transparent;transition: border-color 0.2s;' placeholder='SSID' name='ssid' id='name' required />"
        "<br><br>"
        "<input type='password' style='  font-family: inherit;width: 100%;border: 0;border-bottom: 2px solid gray;outline: 0;font-size: 1.3rem;color: white;padding: 7px 0;background: transparent;transition: border-color 0.2s;' placeholder='PASSWORD' name='pass' id='name' />"
        "<br><br><br>"
        "<input class='boton' type='submit' value='GUARDAR' style='background: linear-gradient(to bottom right, #EF4765, #FF9A5A);border: 0;border-radius: 12px;color: #FFFFFF;cursor: pointer;display: inline-block;font-family: inherit;font-size: 16px;font-weight: 550;line-height: 2.5;outline: transparent;padding: 0 1rem;text-align: center;text-decoration: none;transition: box-shadow .2s ease-in-out;user-select: none;-webkit-user-select: none;touch-action: manipulation;white-space: nowrap; margin-left: 20%;'/><br><br> "
    "</form>"
    "<a href='escanear'><button style='background: linear-gradient(to bottom right, #EF4765, #FF9A5A);border: 0;border-radius: 12px;color: #FFFFFF;cursor: pointer;display: inline-block;font-family: inherit;font-size: 16px;font-weight: 550;line-height: 2.5;outline: transparent;padding: 0 1rem;text-align: center;text-decoration: none;transition: box-shadow .2s ease-in-out;user-select: none;-webkit-user-select: none;touch-action: manipulation;white-space: nowrap;' >ESCANEAR</button></a>";

String paginafin = "</div>"
"</body>"
"</html>";

String InicioDiv = "<div class='box'>";
String FinalDiv = "</div>";


// WIFI SERVER -------------------------------------------------------

WiFiClient espClient;
ESP8266WebServer server(80);


// Void SETUP -------------------------------------------------------

void setup() { 

  //Definimos pines. 
  pinMode(Buzzer, OUTPUT); // Definimos el pin del Buzzer como salida. 
  pinMode(PinAnalogico, INPUT); // Definimos el pin Analogico como entrada.
  pinMode(Boton, INPUT); // Definimos el pin del Boton como entrada.

  Serial.begin(9600); // Iniciamos monitor serial.
  Serial.println(""); 

  EEPROM.begin(512); // Iniciamos la comunicacion con la memoria EEPROM.

  if (digitalRead(14) == 0) // Si el pin del Boton lee un 0 se activa la funcion "modoconf()".
  {
    modoconf(); // Funcion para configurar el wifi a utilizar. 
  }

  leer(0).toCharArray(ssid, 50); // Llama la funcion leer y asigna el valor del SSID. Leemos un strig y lo comvertimos en un charArray de 50 bits.
  leer(50).toCharArray(pass, 50); // Llama la funcion leer y asigna el valor del PASSWORD. Leemos un strig y lo comvertimos en un charArray de 50 bits.

  setup_wifi(); // Iniciamos funcion para que se conecte al WIFI.

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH); // Inicia la base de datos de FIREBASE. 
  Firebase.reconnectWiFi(true); // 

}

// Void LOOP -------------------------------------------------------

void loop() {
  
  int Valor_Analogico = analogRead(PinAnalogico); // Lee el valor analogico .
  int limite = !digitalRead(PinDigital); // Leer el valor digital. el "!" es porque los valores dan invertidos cuando es 0 es porque esta superando el limite y viceversa.
  Serial.print("Valor del CO: ");
  Serial.println(Valor_Analogico); // Imprime los valores de CO.

  //Porcentaje
  int porcentaje = ((Valor_Analogico * 100 ) / 1023);
  Serial.print("Valor en porcentaje : ");
  Serial.println(porcentaje);

  Serial.print("Limite: ");
  Serial.println(limite); // Imprime el limite alcanzado ALTO o BAJO.
  delay(100);


  Firebase.setInt(firebaseD, nodo + "/Valor", porcentaje); // Escribimos en la ruta de Valor el porcentaje.
  // Firebase.setBool(firebaseD, nodo + "/Alarma", limite); // Escribimos en la ruta de Alarma si se enciende el buzzer. 

  Firebase.getBool(firebaseD, nodo + "/OnOff/Estado");

  Serial.println(firebaseD.boolData()); // Leemos lo que que hay adentro de ALARMA.

  if ((firebaseD.boolData()) == true and porcentaje >= 50 )
  {
    digitalWrite(Buzzer, HIGH);
  }
  else{
    digitalWrite(Buzzer, LOW);
   }


  
}


// WIFI_STA -------------------------------------------------------

void setup_wifi() {

  WiFi.mode(WIFI_STA); // Iniciamos el modo ESTACION.
  WiFi.begin(ssid, pass); 
  while (WiFi.status() != WL_CONNECTED and contconexion <50) { //Mientras no se supere el limite de tiempo(50) y no se conecte al wifi ocurre lo del while.
    ++contconexion; // +1 en contconexion.
    delay(250);
    Serial.print(".");
  }

  if (contconexion < 50) {   
      Serial.println("");
      Serial.println("WiFi conectado");
      Serial.println("Mi IP:");
      Serial.println(WiFi.localIP()); // Imprimimos la IP local.
  }

  else { 
      Serial.println("");
      Serial.println("Error de conexion");
  }

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.println("Comprobar SSID y PASSWORD");
  }

  delay(1000);
  
}


// Configuracion de la pagina web -------------------------------------------------------

void paginaconf() {
  server.send(200, "text/html", pagina + InicioDiv + mensaje + FinalDiv + paginafin); 
}


// Modo CONFIGURACION -------------------------------------------------------

void modoconf() {
   
  WiFi.softAP(ssidConf, passConf); // Iniciamos el modo Punto de acceso.
  IPAddress IP = WiFi.softAPIP(); // Guardo la IP en IP
  Serial.print("IP del acces point: ");
  Serial.println(IP);
  Serial.println("Inicio el Web Server");

  server.on("/", paginaconf); // Configuracion de la pagina.

  server.on("/guardar_conf", guardar_conf); //Graba en la eeprom la configuracion.

  server.on("/escanear", escanear); //Escanean las redes wifi disponibles.
  
  server.begin(); // Iniciamos el Server.

  while (true) {
      server.handleClient(); // Loop infinito que siempre funcione la página.
  }
}

// Guardamos la configuracion Wifi -------------------------------------------------------

void guardar_conf() {
  
  Serial.println(server.arg("ssid")); // Imprimimos en el monitor el SSID.
  grabar(0,server.arg("ssid")); // Graba en la posicion 0 el SSID.
  Serial.println(server.arg("pass")); // Imprimimos en el monitor el PASSWORD.
  grabar(50,server.arg("pass")); // Graba en la posicion 50 el PASSWORD.

  mensaje =  "<p style='font-family: inherit'>Configuracion WIFI</p>"; // Guarda en mensaje "..." y vuelve a iniciar  la funcion paginaconf. 


  paginaconf();
}


// Grabar en la EEPROM -------------------------------------------------------

void grabar(int addr, String a) { // Se le pasa dos parametros (Direccion, Palabra a GUARDAR).
  int tamano = a.length();  // Definimos un entero con el tamaño del string, en este caso 50.
  char inchar[50]; // Crea una cadena de caracteres llamada inchar de 50. 
  a.toCharArray(inchar, tamano+1); // Asigna a ese string a ese cadena de caracteres, +1 porque es del 0 - 50 = 51 . 
  for (int i = 0; i < tamano; i++) { 
    EEPROM.write(addr+i, inchar[i]); // Escribimos en la memoria caracter por caracter. 
  }
  for (int i = tamano; i < 50; i++) { 
    EEPROM.write(addr+i, 255); // Cuando supera el tamaño de nuestro string(50) imprime 255 cuando no hay nada.
  }
  EEPROM.commit();
}


// Funcion ESCRIBIR (REPASAR CON TEO) -------------------------------------------------------

String leer(int addr) { // Pasamos un entero con la direccion. 
   byte lectura; // 
   String strlectura;
   for (int i = addr; i < addr+50; i++) {
      lectura = EEPROM.read(i);
      if (lectura != 255) {
        strlectura += (char)lectura;
      }
   }
   return strlectura;
}


// Escanear redes WIFI -------------------------------------------------------
void escanear() {  
  int n = WiFi.scanNetworks(); // Guarda en n el numero de redes encontradas.
  Serial.println("escaneo terminado");
  if (n == 0) { // Si no encuentra redes.
    Serial.println("no se encontraron redes");
    mensaje = "no se encontraron redes";
  }  
  else
  {
    Serial.println("Redes encontradas:");
    Serial.print(n);
    mensaje = "";
    for (int i = 0; i < n; ++i)
    {
      // Guardamos en nesaje las redes encontradas solo con el Nombre de la RED.
      mensaje = (mensaje) + "<p>" + String(i + 1) + ": " + WiFi.SSID(i) + " </p>\r\n";
      delay(10);
    }
    Serial.println(mensaje);
    paginaconf();
  }
}