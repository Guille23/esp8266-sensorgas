// importar funciones de firebase
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue, orderByKey, query  } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
import { getAuth, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyAcN6Yk7To8XIjbPXD3GSULkUSLL0mu7ZY",
  authDomain: "project-1511549768840229133.firebaseapp.com",
  databaseURL: "https://project-1511549768840229133-default-rtdb.firebaseio.com",
  projectId: "project-1511549768840229133",
  storageBucket: "project-1511549768840229133.appspot.com",
  messagingSenderId: "784215418226",
  appId: "1:784215418226:web:4579f332f8fa5908d529eb"
};


const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

let TempRef = document.getElementById("Temp")

let botonOnRef = document.getElementById("incr")
let botonOffRef = document.getElementById("decr")
let AlarmaRef = document.getElementById("Alarma")
let ulHeaderRef = document.getElementById("ul-header")


var correo;
var UID;

onAuthStateChanged(auth, (user) => {
  if (user) {
    const uid = user.uid;
    UID = uid;
    console.log("Usuario actual:" + uid);

    const email = user.email;
    correo=email;
    console.log(email)
    botonOnRef.addEventListener("click", EnviarTrue)
    botonOffRef.addEventListener("click", EnviarFalse)
    ulHeaderRef.innerHTML = `
    <li class="nav-item">
            <button class="btn btn-outline-warning me-3" type="button" id="logout">Cerrar sesión</button>
        </li>
    `
    let logoutRef = document.getElementById("logout")
    logoutRef.addEventListener("click", Logout)

    const RutaRef = ref(database, 'Proyecto/Valor/');
    onValue(RutaRef, (snapshot) => {
      const data = snapshot.val();
      TempRef.innerHTML = `
      <p style="font-size: 100px;">${data}%</p>
      `;
    })

  } else {
    console.log("Ningun usuario detectado ")
    botonOnRef.addEventListener("click", Ventana)
    botonOffRef.addEventListener("click", Ventana)
    TempRef.innerHTML = `
    <p style="font-size: 100px;">0%</p>
    `;
  }
});

function Logout(){
  signOut(auth).then(() => {
    // Sign-out successful.
    location.reload()
  }).catch((error) => {
    // An error happened.
  });
}

function EnviarTrue(){
  set(ref(database, "Proyecto/OnOff/"), {
    Estado: true,

})
}



function EnviarFalse(){
  set(ref(database, "Proyecto/OnOff/"), {
    Estado: false,
})
}

const ON_OFFRef = ref(database, 'Proyecto/OnOff/Estado');
onValue(ON_OFFRef, (snapshot) => {
  const dataON_OFF = snapshot.val();
  if (dataON_OFF == true){
    var dataOnOFF1 = "Estado: Encendido"
    
  }
  else{
    var dataOnOFF1 = "Estado: Apagado"
  }
  // AlarmaRef.innerHTML = ""
  AlarmaRef.innerHTML = `
  <p style = " font-size : 13px;" >${dataOnOFF1}</p>
  `;
})

function Ventana(){
  window.location.href = "pages/Login.html"
}
