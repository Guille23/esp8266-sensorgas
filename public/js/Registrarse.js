import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
  apiKey: "AIzaSyAcN6Yk7To8XIjbPXD3GSULkUSLL0mu7ZY",
  authDomain: "project-1511549768840229133.firebaseapp.com",
  databaseURL: "https://project-1511549768840229133-default-rtdb.firebaseio.com",
  projectId: "project-1511549768840229133",
  storageBucket: "project-1511549768840229133.appspot.com",
  messagingSenderId: "784215418226",
  appId: "1:784215418226:web:4579f332f8fa5908d529eb"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig)

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app)

const database = getDatabase(app)

let correoRefReg = document.getElementById("Email-Registrarse");
let passRefReg = document.getElementById("Password-Registrarse");
let buttonRefReg = document.getElementById("Boton-Registrarse");
let NombreRefReg = document.getElementById("Nombre");

buttonRefReg.addEventListener("click", RegistroUser);

function RegistroUser(){

    if((correoRefReg.value != '') && (passRefReg.value != '') && (NombreRefReg.value != '')){

    createUserWithEmailAndPassword(auth, correoRefReg.value, passRefReg.value, NombreRefReg.value)
  .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
            console.log("Usuario: " + user + " ID: " + user.uid);
            console.log("Creación de usuario.");

            let nombre = NombreRefReg.value


            set(ref(database, "usuarios/" + user.uid), {
                Nombre_Apellido: nombre,
            }).then(() => {
                window.location.href = "../index.html";
            })
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
  });
}
else{
            alert("Revisar que los campos de usuario y contraseña esten completos.");
        }  
}
